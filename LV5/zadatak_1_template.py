import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

colors = np.array([0, 10, 20, 30, 40, 45, 50, 55, 60, 70, 80, 90, 100])

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# inicijalizacija i ucenje modela logisticke regresije
model = LogisticRegression()
model.fit(X_train, y_train)

# predikcija na skupu podataka za testiranje
y_test_p = model.predict(X_test)

thetas = model.coef_

a = -thetas[0,0] / thetas[0,1]
b = -model.intercept_[0] / thetas[0,1]

ray_x = [-4, 4]
ray_y = [a*-4+b, a*4+b]

plt.fill_between(ray_x, ray_y, a*-4+b, interpolate = True)
plt.scatter(X_train[:,0], X_train[:,1], marker = "o", c=y_train, cmap="viridis")
plt.scatter(X_test[:,0], X_test[:,1], marker = "x", c=y_test, cmap="viridis")
plt.show()

# matrica zabune
cm = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()
# report
print (classification_report(y_test, y_test_p))

print(abs(y_test - y_test_p))

plt.scatter(X_test[:,0], X_test[:,1], marker = "x", c=-abs(y_test - y_test_p), cmap = "brg")
plt.show()