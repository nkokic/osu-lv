import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('LV3\\data_C02_emission.csv')

data['Make'] = data['Make'].astype("category")
data['Model'] = data['Model'].astype("category")
data['Vehicle Class'] = data['Vehicle Class'].astype("category")
data['Engine Size (L)'] = data['Engine Size (L)'].astype("float32")
data['Cylinders'] = data['Cylinders'].astype("int32")
data['Transmission'] = data['Transmission'].astype("category")
data['Fuel Type'] = data['Fuel Type'].astype("category")
data['Fuel Consumption City (L/100km)'] = data['Fuel Consumption City (L/100km)'].astype("float32")
data['Fuel Consumption Hwy (L/100km)'] = data['Fuel Consumption Hwy (L/100km)'].astype("float32")
data['Fuel Consumption Comb (L/100km)'] = data['Fuel Consumption Comb (L/100km)'].astype("float32")
data['Fuel Consumption Comb (mpg)'] = data['Fuel Consumption Comb (mpg)'].astype("float32")
data['CO2 Emissions (g/km)'] = data['CO2 Emissions (g/km)'].astype("float32")

plt.hist(data['CO2 Emissions (g/km)'])
plt.show() #sredina se nalazi oko 250 g/km, vrlo mala nekolicina je preko 400 g/km

benzin = data[data['Fuel Type'] == "X"]
premiumBenzin = data[data['Fuel Type'] == "Z"]
diesel = data[data['Fuel Type'] == "D"]
ethanol = data[data['Fuel Type'] == "E"]
gas = data[data['Fuel Type'] == "N"]

plt.scatter(x= benzin['Fuel Consumption City (L/100km)'],y= benzin['CO2 Emissions (g/km)'], c = "red")
plt.scatter(x= premiumBenzin['Fuel Consumption City (L/100km)'],y= premiumBenzin['CO2 Emissions (g/km)'], c = "orange")
plt.scatter(x= diesel['Fuel Consumption City (L/100km)'],y= diesel['CO2 Emissions (g/km)'], c = "brown")
plt.scatter(x= ethanol['Fuel Consumption City (L/100km)'],y= ethanol['CO2 Emissions (g/km)'], c = "blue")
plt.scatter(x= gas['Fuel Consumption City (L/100km)'],y= gas['CO2 Emissions (g/km)'], c = "black")

plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("CO2 Emissions (g/km)")
plt.show() #po svim tipovima goriva, točke prate linearni rast; auti na ethanol imaju najmanji rast CO2 emisije po potrošnji goriva naspram ostalih vozila