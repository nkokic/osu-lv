import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('LV3\\data_C02_emission.csv')

#sveukupno 2212 podataka

data['Make'] = data['Make'].astype("category")
data['Model'] = data['Model'].astype("category")
data['Vehicle Class'] = data['Vehicle Class'].astype("category")
data['Engine Size (L)'] = data['Engine Size (L)'].astype("float32")
data['Cylinders'] = data['Cylinders'].astype("int32")
data['Transmission'] = data['Transmission'].astype("category")
data['Fuel Type'] = data['Fuel Type'].astype("category")
data['Fuel Consumption City (L/100km)'] = data['Fuel Consumption City (L/100km)'].astype("float32")
data['Fuel Consumption Hwy (L/100km)'] = data['Fuel Consumption Hwy (L/100km)'].astype("float32")
data['Fuel Consumption Comb (L/100km)'] = data['Fuel Consumption Comb (L/100km)'].astype("float32")
data['Fuel Consumption Comb (mpg)'] = data['Fuel Consumption Comb (mpg)'].astype("float32")
data['CO2 Emissions (g/km)'] = data['CO2 Emissions (g/km)'].astype("float32")

print(data)

data.drop_duplicates()
data.dropna(axis=0)

print(data)

top3eco = data.sort_values(by="Fuel Consumption City (L/100km)", ascending=True).head(3)

print(top3eco.loc[:,['Make', 'Model', 'Fuel Consumption City (L/100km)']])

litersData = data.loc[:, ['Make', 'Model', 'Engine Size (L)', 'CO2 Emissions (g/km)']]
litersData = litersData[litersData['Engine Size (L)'] < 3.5]
litersData = litersData[litersData['Engine Size (L)'] > 2.5]

print(litersData) #471 rows -> 471 auta s navedenom veličinom motora

print(f"Average C02 emission <2.5 , 3.5>(L): {litersData['CO2 Emissions (g/km)'].aggregate('mean')} g/km")

audis = data[data['Make'] == "Audi"]

print(f"There are {audis['Make'].aggregate('count')} measured audis")

audis4 = audis[audis['Cylinders'] == 4]

print(f"Average 4 cyllinder audi emission: {audis4['CO2 Emissions (g/km)'].aggregate('mean')} g/km") 

cylindersGroups = data.groupby('Cylinders').agg({'CO2 Emissions (g/km)': 'mean'})

print(cylindersGroups)

dizel = data[data['Fuel Type'] == "D"]
benzin = data[data['Fuel Type'] == "X"]

print(f"Benzine average fuel consumption: {benzin['Fuel Consumption City (L/100km)'].mean()}")
print(f"Benzine middle fuel consumption: {benzin['Fuel Consumption City (L/100km)'].median()}")
print(f"Diesel average fuel consumption: {dizel['Fuel Consumption City (L/100km)'].mean()}")
print(f"Diesel middle fuel consumption: {dizel['Fuel Consumption City (L/100km)'].median()}")

specificVehicle = data[data['Cylinders'] == 4]
specificVehicle = specificVehicle[specificVehicle['Fuel Type'] == "D"]
specificVehicle = specificVehicle.sort_values('Fuel Consumption City (L/100km)', ascending= False)
specificVehicle = specificVehicle.loc[:, ['Make', 'Model', 'Cylinders', 'Fuel Consumption City (L/100km)']]
print("Most spending 4 cylinder diesel car:")
print(specificVehicle.head(1))

manuals = data.loc[:, ['Make', 'Model', 'Transmission']]
manuals = manuals[manuals['Transmission'].astype(str).str[0] == "M"]
print(manuals)

numerics = data.loc[:, ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)' ,'CO2 Emissions (g/km)']]

print(numerics.corr())
