import numpy as np
import matplotlib.pyplot as plt

white = np.full([50,50], 255)
black = np.full([50,50], 0)

img = np.block([[black, white], [white, black]])

plt.figure()
plt.imshow(img, cmap = "gray")
plt.show()