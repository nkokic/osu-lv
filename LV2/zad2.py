import numpy as np
import matplotlib.pyplot as plt

file = open("data.csv")
data = list()

for line in file:
    line = line.strip()
    d = line.split(",")
    if(d[0] == '1.0'):
        d[0] = 1.0
    elif(d[0] == '0.0'):
        d[0] = 0.0
    else:
        continue

    data.append([d[0], float(d[1]), float(d[2])])

print(f"Na {len(data)} osoba je izvršeno mjerenja.")
print(data)

data = np.array(data)
print(data)


plt.scatter(data[:,2], data[:,1])
plt.xlabel("Masa")
plt.ylabel("Visina")
plt.show()

plt.scatter(data[::50,2], data[::50,1])
plt.xlabel("Masa")
plt.ylabel("Visina")
plt.show()
print("Visine:")
print(f"    Max: {np.max(data[:,1])}")
print(f"    Min: {np.min(data[:,1])}") 
print(f"    Avg: {np.average(data[:,1])}")

mData = data[(data[:,0] == 1.0)]

print(mData)

print("Visine (samo M):")
print(f"    Max: {np.max(mData[:,1])}")
print(f"    Min: {np.min(mData[:,1])}") 
print(f"    Avg: {np.average(mData[:,1])}")
