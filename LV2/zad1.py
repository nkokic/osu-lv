import numpy as np
import matplotlib.pyplot as plt

x = [1, 2, 3, 3]
y = [1, 2, 2, 1]

plt.plot([1, 3], [1, 1], 'orange', linewidth = 2, marker="", markersize=10)
plt.plot(x, y, 'r', linewidth = 2, marker=".", markersize=10)

plt.axis([0, 4, 0, 4])
plt.xlabel("x")
plt.ylabel("y")
plt.title("shape")
plt.show()