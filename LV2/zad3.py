import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()

bright_img = img + np.full(img.shape, 70)

bright_img[bright_img > 255] = 255

plt.figure()
plt.imshow(bright_img, cmap = "gray")
plt.show()

img = img[:,int(1/4*640):int(1/2*640)]

plt.figure()
plt.imshow(img, cmap = "gray")
plt.show()

img = plt.imread("road.jpg")
img = img[:,:,0].copy()
img = img[::-1,:].transpose()

plt.figure()
plt.imshow(img, cmap = "gray")
plt.show()

img = plt.imread("road.jpg")
img = img[:,:,0].copy()
img = img[:,::-1]

plt.figure()
plt.imshow(img, cmap = "gray")
plt.show()