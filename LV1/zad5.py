SMSes = open("SMSSpamCollection.txt")
hamWordCounts = []
spamWordCounts = []
spamQ = 0

for line in SMSes:
    line = line.rstrip()
    words = line.split()
    if(words[0] == "ham"):
        words.pop(0)
        n = 0
        for word in words:
            n += 1
        hamWordCounts.append(n)
    elif(words[0] == "spam"):
        words.pop(0)
        n = 0
        for word in words:
            n += 1
        spamWordCounts.append(n)

        if(line[-1] == '!'):
            spamQ += 1

SMSes.close()

sumHam = 0
sumSpam = 0
for ham in hamWordCounts:
    sumHam += ham
for spam in spamWordCounts:
    sumSpam += spam
print(f"Average ham word count: {sumHam / len(hamWordCounts)}")
print(f"Average spam word count: {sumSpam / len(spamWordCounts)}")
print(spamQ)