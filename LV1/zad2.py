import math

grades = ['A', 'B', 'C', 'D', 'F']

percent = 0

try:
    percent = float(input("Percent: "))
except:
    print("Invalid input. Expected a number.")

if(percent > 1 or percent < 0):
    print("Number out of range [0-1]")
elif(percent >= .9):
    print("A")
elif(percent >= .8):
    print("B")
elif(percent >= .7):
    print("C")
elif(percent >= .6):
    print("D")
else:
    print("F")