text = open("song.txt")
wordCounts = {}
for line in text:
    for word in line.split():
        if wordCounts.get(word) == None:
            wordCounts[word] = 1
        else:
            wordCounts[word] += 1

text.close()
        
print(wordCounts)