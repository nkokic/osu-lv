import numpy as np
from tensorflow import keras
from keras import layers
from keras.models import load_model
from matplotlib import pyplot as plt

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


x_train_s = np.reshape(x_train_s, (60000, 28*28))
x_test_s = np.reshape(x_test_s, (10000, 28*28))

model = load_model("MODEL/")
predictions = model.predict(x_test_s)
predictions = np.argmax(predictions, axis=1)


for i in range(10000):
    if(predictions[i] != y_train[i]):
        image = x_train[i]
        plt.imshow(image)
        plt.title(f"Real: {y_train[i]}, Prediction: {predictions[i]}")
        plt.show()