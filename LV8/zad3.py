import numpy as np
from tensorflow import keras
from keras import layers
from keras.models import load_model
from matplotlib import pyplot as plt
from matplotlib import image as img



image = img.imread("LV8/test.png")
image = image[:,:,0]

image = 1 - image

x_test = image.astype("float32") / 255
x_test = np.reshape(x_test, (28*28, ))
x_test = np.array([x_test])
print(x_test.ndim)

model = load_model("MODEL/")
prediction = model.predict(x_test)
prediction = np.argmax(prediction, axis=1)

plt.imshow(image)
plt.title(f"Prediction: {prediction[0]}")
plt.show()
