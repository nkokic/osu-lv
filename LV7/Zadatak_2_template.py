import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
ex = 1
img = Image.imread(f"LV7\\imgs\\test_{ex}.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title(f"Originalna slika {ex}")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
X = img_array.copy()

#grupiranje podataka
elbow = list()
elbow.append(np.Infinity)
for k in range(1, 7):
    print(f"{(k-1)/6*100}%")
    km = KMeans(n_clusters=k , init='k-means++', n_init=5, random_state=0)
    km.fit(X)
    elbow.append(km.inertia_)
print("100%")
plt.title(f"Učikovitost modela")
plt.xlabel("K")
plt.ylabel("Entropija")
plt.plot(elbow)
plt.show()
Y = km.predict(X)
colors = km.cluster_centers_

X[:] = colors[Y]

new_img = np.reshape(X, (w, h, d))

plt.title(f"Modificirana slika {ex} s {k} boje")
plt.imshow(new_img)
plt.tight_layout()
plt.show()