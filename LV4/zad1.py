import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
import sklearn.linear_model as lm
from sklearn . metrics import mean_absolute_error


data = pd.read_csv('LV3\\data_C02_emission.csv')

input_var = data[['Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders']]
output_var = data[['CO2 Emissions (g/km)']]

#6 parametara => MAE = 8.18
#1 parametara ['Fuel Consumption City (L/100km)'] => MAE = 11.23
#što više ulaznih veličina to veća preciznost

X = input_var.to_numpy()
y = output_var.to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

print(f"Train podaci: {len(X_train)}")
print(f"Test podaci: {len(X_test)}")

plt.scatter(X_train[:,0], y_train, c="blue")
plt.scatter(X_test[:,0], y_test, c="red")
plt.show()

# min - max skaliranje
sc = MinMaxScaler ()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

plt.hist(X_train[:,0])
plt.hist(X_train_n[:,0])
plt.show()

model = lm.LinearRegression()
model.fit(X_train_n, y_train)

thetas = model.coef_
print(f"Thetas: {thetas}")

y_test_p = model.predict(X_test_n)

plt.scatter(X_test_n[:,0], y_test, c="blue", label="real values")
plt.scatter(X_test_n[:,0], y_test_p, c="red", label="predicted values")
plt.legend()
plt.show()

MAE = mean_absolute_error(y_test, y_test_p)

print(f"Mean absolute error: {MAE}")