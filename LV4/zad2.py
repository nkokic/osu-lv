import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
import numpy


data = pd.read_csv('LV3\\data_C02_emission.csv')

X = data[['Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders']]
ohe = OneHotEncoder() 
input_var_cat = ohe.fit_transform(data[['Fuel Type']]).toarray()
output_var = data[['CO2 Emissions (g/km)']]
y = output_var.to_numpy()

X = pd.concat([X, pd.DataFrame(input_var_cat, columns=ohe.get_feature_names_out(["Fuel Type"]))], axis = 1)

print(X)
X = X.to_numpy()
print(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

print(f"Train podaci: {len(X_train)}")
print(f"Test podaci: {len(X_test)}")

plt.scatter(X_train[:,0], y_train, c="blue")
plt.scatter(X_test[:,0], y_test, c="red")
plt.show()

# min - max skaliranje
sc = MinMaxScaler ()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

plt.hist(X_train[:,0])
plt.hist(X_train_n[:,0])
plt.show()

model = lm.LinearRegression()
model.fit(X_train_n, y_train)

thetas = model.coef_
print(f"Thetas: {thetas}")

y_test_p = model.predict(X_test_n)

plt.scatter(X_test_n[:,0], y_test, c="blue", label="real values")
plt.scatter(X_test_n[:,0], y_test_p, c="red", label="predicted values")
plt.legend()
plt.show()

MAE = mean_absolute_error(y_test, y_test_p)

print(f"Mean absolute error: {MAE}") 
#MAE = 1.41
#MAE je manji od zad1
#model iz zadatka 2 je precizniji od modela iz zadatka 1

absoluteDeltas = abs(y_test_p - y_test)

print(X_test[numpy.argmin(absoluteDeltas)])